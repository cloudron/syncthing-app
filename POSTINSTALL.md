<nosso>
This app is pre-setup with an account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme<br/>

Please change the admin password immediately.
</nosso>

<sso>
Note that all users manage and synchronize the same set of files.
</sso>

