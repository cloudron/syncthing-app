#!/bin/bash

set -eu

mkdir -p /app/data/config /app/data/folders /run/syncthing

# if this if the first run, generate a useful config
if [[ ! -f /app/data/config/config.xml ]]; then
    echo "=> Generating config"
    STNODEFAULTFOLDER=1 /app/code/syncthing --generate="/app/data/config"

    # The password value (changeme) was determined by reading config.xml and setting value in the GUI
    # urAccepted is 0 for not decided, -1 for no reporting
    xmlstarlet ed --inplace \
        --subnode "//configuration/gui" -t elem -n user -v "admin" \
        --subnode "//configuration/gui" -t elem -n password -v '$2a$10$93OsgwAMD4NcPFTpGDnuTORMMbj5dc1ZlYUPrQCcy1N/Bsy9scUvK' \
        --update "//configuration/options/defaultFolderPath" -v '/app/data/folders' \
        --update "//configuration/options/urAccepted" -v '-1' \
        /app/data/config/config.xml

fi

if [[ -n ${CLOUDRON_LDAP_HOST:-} ]]; then
    xmlstarlet ed --inplace \
        --subnode "//configuration/gui" -t elem -n "authMode" -v "ldap" \
        --subnode "//configuration/ldap" -t elem -n "address" -v "${CLOUDRON_LDAP_HOST:-}:${CLOUDRON_LDAP_PORT}" \
        --subnode "//configuration/ldap" -t elem -n "bindDN" -v "${CLOUDRON_LDAP_BIND_DN:-}" \
        --subnode "//configuration/ldap" -t elem -n "insecureSkipVerify" -v "true" \
        --subnode "//configuration/ldap" -t elem -n "searchBaseDN" -v "${CLOUDRON_LDAP_USERS_BASE_DN:-}" \
        --subnode "//configuration/ldap" -t elem -n "searchFilter" -v "(&amp;(objectclass=user)(|(username=%s)(mail=%s)))" \
        /app/data/config/config.xml
fi


# Set the listenAddress and the gui enabled to make sure user doesnt lock themselves  out by accident.
sed -e "s,<listenAddress>.*</listenAddress>,<listenAddress>tcp://:${SYNC_PORT}</listenAddress>," -i /app/data/config/config.xml
sed -e 's,<gui .*>,<gui enabled="true" tls="false" debugging="false">,' -i /app/data/config/config.xml

chown -R cloudron:cloudron /app/data /run/syncthing

echo "Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i syncthing
