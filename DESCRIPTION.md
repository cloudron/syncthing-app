Syncthing replaces proprietary sync and cloud services with something open, trustworthy and decentralized. Your data is your data alone and you deserve to choose where it is stored, if it is shared with some third party and how it's transmitted over the Internet.

### Features

#### Secure & Private

- **Private.** None of your data is ever stored anywhere else other than on your computers. There is no central server that might be compromised, legally or illegally.
- **Encrypted.** All communication is secured using TLS. The encryption used includes perfect forward secrecy to prevent any eavesdropper from ever gaining access to your data.
- **Authenticated.** Every node is identified by a strong cryptographic certificate. Only nodes you have explicitly allowed can connect to your cluster.

#### Easy to Use

Syncthing is still in development, although a large number of features have already been implemented:

- **Web GUI.** Configure and monitor Syncthing via a responsive and powerful interface accessible via your browser.
- **Portable.** Works on Mac OS X, Windows, Linux, FreeBSD, Solaris and OpenBSD. Run it on your desktop computers and synchronize them with your server for backup.
- **Simple.** Syncthing doesn't need IP addresses or advanced configuration: it just works, over LAN and over the Internet. Every machine is identified by an ID. Just give your ID to your friends, share a folder and watch: UPnP will do if you don't want to port forward or you don't know how.
- **Powerful.** Synchronize as many folders as you need with different people.

#### Native GUIs & Integrations

- **Windows** tray utility, filesystem watcher & launcher: [SyncTrayzor](https://github.com/canton7/SyncTrayzor/releases/latest)
- **Cross-platform** GUI wrapper: [Syncthing-GTK](https://github.com/syncthing/syncthing-gtk/releases/latest)
- **Android** app: [Syncthing App](https://f-droid.org/repository/browse/?fdid=com.nutomic.syncthingandroid)
