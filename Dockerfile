FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=syncthing/syncthing versioning=semver extractVersion=^v(?<version>.+)$
ARG SYNCTHING_VERSION=1.29.3

RUN wget https://github.com/syncthing/syncthing/releases/download/v${SYNCTHING_VERSION}/syncthing-linux-amd64-v${SYNCTHING_VERSION}.tar.gz -O - | tar -xz -C /app/code --strip-components=1

# add supervisor configs
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/syncthing/supervisord.log /var/log/supervisor/supervisord.log

ADD nginx.conf start.sh /app/code/

CMD [ "/app/code/start.sh" ]
